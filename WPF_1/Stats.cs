﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.ServiceProcess;
namespace WPF_1
{
    class Stats
    {
        public List<string> Processes
        {
            get
            {
                return _processes;
            }
            set
            {
                Processes = _processes;
            }
        }

        List<string> _processes;

        public List<string> GetAllProcesses()
        {
            Process[] procs = Process.GetProcesses();
            List<string> pName = new List<string>();
            foreach (Process p in procs) pName.Add(p.ProcessName);
            
            _processes = pName;

            return _processes;
        }
        public ServiceController[] GetAllServices()
        {
            List<ServiceController> servicesList = new List<ServiceController>();
            ServiceController[] services = ServiceController.GetServices();
            foreach (ServiceController s in services) servicesList.Add(s);
            return services;


        }

    }
}
