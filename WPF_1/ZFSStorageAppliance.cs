﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;

namespace WPF_1
{
    class ZFSStorageAppliance
    {
        //private List<ZPool> _zpools;
        public class Creds
        {
            public Creds(string username, string password)
            {
                Username = username;
                Password = password;
            }
            public string Username;
            public string Password;
        }


        public ZFSStorageAppliance(string IP, string username, string password)
        {

            IPAddress = IP;
            Credentials = new Creds(username, password);

        }

        public string Name { set; get; }
        public string IPAddress { set; get; }
        public Creds Credentials { set; get; }
        public List<ZPool> ZPools
        {
            get
            {
                List<ZPool> zpools = new List<ZPool>();
                SshClient client = new SshClient(IPAddress, Credentials.Username, Credentials.Password);
                client.Connect();
                SshCommand command = client.RunCommand("zpool list -o name");
                string commandResultsString = command.Result;
                List<string> commandResultsList = new List<string>(commandResultsString.Split());

                commandResultsList.RemoveAt(0);
                commandResultsList.RemoveAt(commandResultsList.Count - 1);

                foreach (string name in commandResultsList)
                {
                    ZPool pool = new ZPool(name, this);
                    zpools.Add(pool);
                }
                foreach (string name in commandResultsList)
                {
                    ZPoolStatusCollector pool = new ZPoolStatusCollector(name, this);
                    ZPoolStatus test = pool.GetZPoolStatus(name);
                    // foreach (string line in test) Console.WriteLine(line);
                }

                return zpools;
            }
        }
    }
}
