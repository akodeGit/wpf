﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;

namespace WPF_1
{
    class ZPool
    {
        private ZFSStorageAppliance _owningController;
        public ZPool(string name, ZFSStorageAppliance controller)
        {
            Name = name;
            _owningController = controller;
        }
        public string Name { private set; get; }

        public ZPoolStatus PoolStatus { get; set; }

        public string Allocated { get { return getProperty("allocated"); } }
        public string Capacity { get { return getProperty("capacity"); } }
        public string DedupRatio { get { return getProperty("dedupratio"); } }
        public string ExpandSize { get { return getProperty("expandsize"); } }
        public string Free { get { return getProperty("free"); } }
        public string Freeing { get { return getProperty("freeing"); } }
        public string GUID { get { return getProperty("guid"); } }
        public string Health { get { return getProperty("health"); } }
        public string Size { get { return getProperty("size"); } }
        public string Altroot { get { return getProperty("altroot"); } }
        public string Autoexpand { get { return getProperty("autoexpand"); } }
        public string Autoreplace { get { return getProperty("autoreplace"); } }
        public string Bootfs { get { return getProperty("bootfs"); } }
        public string Cachefile { get { return getProperty("cachefile"); } }
        public string Comment { get { return getProperty("comment"); } }
        public string DedupDitto { get { return getProperty("dedupditto"); } }
        public string Delegation { get { return getProperty("delegation"); } }
        public string Failmode { get { return getProperty("failmode"); } }
        public string ListSnapshots { get { return getProperty("listsnapshots"); } }
        public string Readonly { get { return getProperty("readonly"); } }
        public string Version { get { return getProperty("version"); } }

        private string getProperty(string propertyName)
        {
            SshClient client = new SshClient(_owningController.IPAddress, _owningController.Credentials.Username, _owningController.Credentials.Password);
            client.Connect();
            string commandString = String.Format("zpool list -o {0} {1}", propertyName, Name);
            SshCommand command = client.RunCommand(commandString);
            List<string> commandResultsList = new List<string>(command.Result.Split('\r', '\n'));
            string propertValueString = commandResultsList[1].Trim();

            return propertValueString;
        }
    }
}
