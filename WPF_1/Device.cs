﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_1
{
    class Device
    {
        public string Name { get; set; }
        public string State { get; set; }
        public int Read { get; set; }
        public int Write { get; set; }
        public int Cksum { get; set; }
    }
}
