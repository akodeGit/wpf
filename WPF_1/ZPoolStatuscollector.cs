﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;

namespace WPF_1
{
    class ZPoolStatusCollector
    {
         string[] vDevTypes = new string[] { "mirror*", "raidz", "raidz2" };


        private ZFSStorageAppliance _owningController;
        public ZPoolStatusCollector(string name, ZFSStorageAppliance controller)
        {
            _owningController = controller;

        }

        public ZPoolStatus GetZPoolStatus(string pool)
        {
            ZPoolStatus poolStatus = new ZPoolStatus();


            SshClient client = new SshClient(_owningController.IPAddress, _owningController.Credentials.Username, _owningController.Credentials.Password);
            client.Connect();
            string commandString = String.Format("zpool status {0}", pool);
            SshCommand command = client.RunCommand(commandString);
            string commandResults = command.Result;
            List<string> commandResultsList = new List<string>(command.Result.Split('\r', '\n'));
            string[] lines = commandResults.Split('\r', '\n');
            List<string> statusLinesList = new List<string>();
            foreach (string line in lines) if(line.Length > 0) statusLinesList.Add(line);
            statusLinesList.RemoveRange(0, 2);
            //foreach (string line in statusLinesList) Console.WriteLine(line);

            List<string> format = new List<string>();
            SshClient client1 = new SshClient(_owningController.IPAddress, _owningController.Credentials.Username, _owningController.Credentials.Password);
            client1.Connect();
            string commandString1 = String.Format(@"echo | format");
            SshCommand command1 = client1.RunCommand(commandString1);
            string commandResults1 = command1.Result;
            Console.WriteLine(commandResults1);
   
            var poolstatus = statusLinesList.FirstOrDefault(p => p.Contains(pool));
            var scan = statusLinesList.FirstOrDefault(p => p.Contains(@"scan: "));
           Console.WriteLine(scan);
           // Console.WriteLine(poolstatus);
            
            return poolStatus;

        }
        private List<string> GetFormatOutPut()
        {
            List<string> format = new List<string>();
            SshClient client = new SshClient(_owningController.IPAddress, _owningController.Credentials.Username, _owningController.Credentials.Password);
            client.Connect();
            string commandString = String.Format(@"echo | format");
            SshCommand command = client.RunCommand(commandString);
            string commandResults = command.Result;


            return format;


        }
    }
}
