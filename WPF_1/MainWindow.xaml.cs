﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Stats stats;
        public MainWindow()
        {
            InitializeComponent();
            stats = new Stats();
            CollectionViewSource itemCollectionViewSource;
            itemCollectionViewSource = (CollectionViewSource)(FindResource("ItemCollectionViewSource"));

            ZFSStorageAppliance storageAppliance = new ZFSStorageAppliance(@"192.168.10.21", @"root", @"fresno80");


            this.combobox_processes.ItemsSource = stats.GetAllServices().Select(x => x.ServiceName).ToList<string>();
            itemCollectionViewSource.Source = storageAppliance.ZPools;
        }

       
    }
}
